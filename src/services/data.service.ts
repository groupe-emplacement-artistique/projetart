import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Idata } from './data.interface.service';
@Injectable({
    providedIn: 'root'
})
export class DataService {
    private localUrl : string = '../assets/data/data.json';

    
    constructor(private http: HttpClient) {}

    public getData(): Observable<Idata[]>{
        return this.http.get<Idata[]>(this.localUrl);
    }


}

//https://www.youtube.com/watch?v=3IJlbUOMw0Y 
//https://www.youtube.com/watch?v=HKrl1cCj5Ik