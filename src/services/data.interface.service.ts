export interface Idata {
    type: string;
    properties: {
        gml_id: number;
        id: number;
        lieux: string;
        adresse: string;
        ville: string;
        artiste: string;
        titre: string;
        description: string;
        photo: string;
        lien: string;
        code_postal: number;
        code_insee: number;
        format: string;
    }
    geometry: {
        type: string;
        coordinates: [number, number];
    }

}