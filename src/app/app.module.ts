import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Router, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { CLesMenusComponent } from './c-les-menus/c-les-menus.component';
import { HttpClientModule } from '@angular/common/http';
import { CBarreNavComponent } from './c-barre-nav/c-barre-nav.component';
import { CMapComponent } from './c-map/c-map.component';

const routes: Routes = [
  { path: 'map', component: CMapComponent },
  { path: 'accueil', component: CLesMenusComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    CLesMenusComponent,
    CBarreNavComponent,
    CMapComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(routes)
  ],
  
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

// { path: 'home', component: HomeComponent },             //vers acceuil
// { path: '', redirectTo: 'home', pathMatch: 'full' },    //retour vers page d'acceuil
// { path: 'c-map/:id', component: CMapComponent },        //direction de la map
// { path: 'c-les-menus', component: CLesMenusComponent},  //direction les menus
// { path: '**', redirectTo: 'home', pathMatch: 'full' },  //si la route est inconnu ex: localhost:4300/hjvbhjbvd